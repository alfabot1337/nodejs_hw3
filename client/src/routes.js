import { Switch, Route, Redirect } from 'react-router-dom';
import { ActiveLoadsPage } from './pages/ActiveLoadsPage';
import { AllLoads } from './pages/AllLoads';
import { AssignedLoads } from './pages/AssignedLoads';
import { AuthPage } from './pages/AuthPage';
import { CreateLoadPage } from './pages/CreateLoadPage';
import { CreateTruckPage } from './pages/CreateTruckPage';
import { LoadPage } from './pages/LoadPage';
import { NewLoads } from './pages/NewLoads';
import { PostedLoads } from './pages/PostedLoads';
import { ProfilePage } from './pages/ProfilePage';
import { TrucksPage } from './pages/TrucksPage';

export const useRoutes = (isAuthenticated, role) => {
	if (isAuthenticated && role === 'SHIPPER') {
		return (
			<Switch>
				<Route exact path="/users/me">
					<ProfilePage />
				</Route>
				<Route exact path="/loads/create">
					<CreateLoadPage />
				</Route>
				<Route exact path="/loads">
					<NewLoads />
				</Route>
				<Route exact path="/loads/posted">
					<PostedLoads />
				</Route>
				<Route exact path="/loads/assigned">
					<AssignedLoads />
				</Route>
				<Route exact path="/loads/history">
					<AllLoads />
				</Route>
				<Route exact path="/loads/:id/view">
					<LoadPage />
				</Route>
				<Route exact path="/loads/:id/edit">
					<LoadPage isEdit={true} />
				</Route>
				<Redirect to="/loads" />
			</Switch>
		);
	}
	if (isAuthenticated && role === 'DRIVER') {
		return (
			<Switch>
				<Route exact path="/users/me">
					<ProfilePage />
				</Route>
				<Route exact path="/trucks/add">
					<CreateTruckPage />
				</Route>
				<Route exact path="/trucks">
					<TrucksPage />
				</Route>
				<Route exact path="/loads/active">
					<ActiveLoadsPage />
				</Route>
				<Route exact path="/loads/:id/view">
					<LoadPage />
				</Route>
				<Redirect to="/trucks" />
			</Switch>
		);
	}
	return (
		<Switch>
			<Route exact path="/">
				<AuthPage />
			</Route>
			<Redirect to="/" />
		</Switch>
	);
};
