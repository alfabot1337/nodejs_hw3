import { useCallback, useEffect, useState } from 'react';
import { useHttp } from './http.hook';

const storageName = 'userData';

export const useAuth = () => {
	const [token, setToken] = useState(null);
	const [email, setEmail] = useState(null);
	const [role, setRole] = useState(null);
	const { request } = useHttp();

	const getUser = useCallback(async () => {
		try {
			const { email, role } = await request('/api/users/me', 'GET', null, {
				Authorization: `Bearer ${token}`,
			});
			setEmail(email);
			setRole(role);
		} catch (e) {}
	}, [request, token]);

	const login = useCallback(
		async jwtToken => {
			setToken(jwtToken);
			localStorage.setItem(
				storageName,
				JSON.stringify({
					token: jwtToken,
				})
			);
			await getUser();
		},
		[getUser]
	);

	const logout = useCallback(() => {
		setToken(null);
		localStorage.removeItem(storageName);
		window.location.reload();
	}, []);

	useEffect(() => {
		const data = JSON.parse(localStorage.getItem(storageName));
		if (data && data.token) {
			login(data.token);
		}
	}, [login, token]);

	return { token, login, logout, email, role };
};
