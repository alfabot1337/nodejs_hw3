import { useContext } from 'react';
import { NavLink, useHistory } from 'react-router-dom';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';

export const Load = ({ load }) => {
	const { request } = useHttp();
	const { token, role } = useContext(AuthContext);
	const history = useHistory();
	const {
		name,
		pickup_address,
		delivery_address,
		created_date,
		status,
		state = 'Unknown',
	} = load;

	const getDate = () => {
		const date = new Date(Date.parse(created_date));
		return `${date.toDateString()} ${date.getHours()}:${date.getMinutes()}`;
	};

	const nextStateHandler = async () => {
		try {
			await request('/api/loads/active/state', 'PATCH', null, {
				Authorization: `Bearer ${token}`,
			});
			history.push('/');
		} catch (error) {}
	};

	return (
		<tr key={load._id}>
			<td>{name}</td>
			<td>{getDate()}</td>
			<td>{pickup_address}</td>
			<td>{delivery_address}</td>
			<td>{status}</td>
			<td>{role === 'DRIVER' && state}</td>
			{role === 'DRIVER' && (
				<td>
					<button onClick={nextStateHandler}>Next State</button>
				</td>
			)}

			<td>
				{load.status === 'NEW' ? (
					<NavLink to={`/loads/${load._id}/edit`}>Edit</NavLink>
				) : null}
			</td>
			<td>
				<NavLink to={`/loads/${load._id}/view`}>View</NavLink>
			</td>
		</tr>
	);
};
