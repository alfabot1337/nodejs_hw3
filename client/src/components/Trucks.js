import { useCallback, useContext, useEffect, useState } from 'react';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';
import { Truck } from './Truck';

export const Trucks = () => {
	const { request, loading } = useHttp();
	const { token } = useContext(AuthContext);
	const [trucks, setTrucks] = useState([]);
	const [hasAssignedTrucks, setHasAssignedTrucks] = useState(false);

	const getTrucks = useCallback(async () => {
		const data = await request('/api/trucks', 'GET', null, {
			Authorization: `Bearer ${token}`,
		});
		return data;
	}, [request, token]);

	useEffect(() => {
		const fetchData = async () => {
			const { trucks } = await getTrucks();
			setTrucks(trucks);
			setHasAssignedTrucks(!!trucks.find(i => i.assigned_to));
		};
		fetchData();
	}, [getTrucks]);

	const renderTrucks = () =>
		trucks
			.map(i => (
				<Truck truck={i} key={i._id} hasAssignedTrucks={hasAssignedTrucks} />
			))
			.reverse();

	if (loading) {
		return <h2>Loading, please wait...</h2>;
	}

	return (
		<>
			<table className="table">
				<thead>
					<tr>
						<th scope="row">Type</th>
						<th scope="row">Assigned to</th>
						<th scope="row">Status</th>
						<th scope="row">Created date</th>
						<th scope="row"></th>
					</tr>
				</thead>
				<tbody>{renderTrucks()}</tbody>
			</table>
		</>
	);
};
