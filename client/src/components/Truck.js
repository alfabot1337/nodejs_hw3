import { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';

export const Truck = ({ truck, hasAssignedTrucks }) => {
	const { assigned_to = 'None', type, status, created_date } = truck;
	const { request, loading } = useHttp();
	const { token } = useContext(AuthContext);
	const history = useHistory();
	const [isEdit, setIsEdit] = useState(false);
	const [truckType, setTruckType] = useState(type);

	const typeHandler = e => {
		setTruckType(e.target.value);
	};

	const getDate = () => {
		const date = new Date(Date.parse(created_date));
		return `${date.toDateString()} ${date.getHours()}:${date.getMinutes()}`;
	};

	const assignTruck = async () => {
		try {
			await request(`/api/trucks/${truck._id}/assign`, 'POST', null, {
				Authorization: `Bearer ${token}`,
			});
		} catch (error) {}
	};

	const deleteTruckHandler = async () => {
		try {
			await request(`/api/trucks/${truck._id}`, 'DELETE', null, {
				Authorization: `Bearer ${token}`,
			});
			history.push('/');
		} catch (error) {}
	};

	const updateTruckHandler = async () => {
		try {
			await request(
				`/api/trucks/${truck._id}`,
				'PUT',
				{ type: truckType },
				{
					Authorization: `Bearer ${token}`,
				}
			);
			truck.type = truckType;
			setIsEdit(false);
		} catch (error) {}
	};

	const changeButtons = (
		<>
			<button
				type="button"
				className="btn btn-success mr-1"
				onClick={assignTruck}
				disabled={hasAssignedTrucks}
			>
				Assign
			</button>
			<button
				type="button"
				className="btn btn-primary mr-1"
				onClick={() => setIsEdit(true)}
				disabled={truck.assigned_to}
			>
				Edit
			</button>
			<button
				type="button"
				className="btn btn-danger"
				disabled={truck.assigned_to}
				onClick={deleteTruckHandler}
			>
				Delete
			</button>
		</>
	);
	const updateButtons = (
		<>
			<button
				type="button"
				className="btn btn-success mr-1"
				onClick={updateTruckHandler}
			>
				Update
			</button>
			<button
				type="button"
				className="btn btn-danger"
				onClick={() => setIsEdit(false)}
			>
				Cancel
			</button>
		</>
	);

	if (loading) {
		return <p>Loading...</p>;
	}

	return (
		<tr key={truck._id}>
			<td>
				{isEdit ? (
					<select
						className="custom-select"
						onChange={typeHandler}
						value={truckType}
					>
						<option value="SPRINTER">Sprinter</option>
						<option value="SMALL STRAIGHT">Small Straight</option>
						<option value="LARGE STRAIGHT">Large Straight</option>
					</select>
				) : (
					type
				)}
			</td>
			<td>{assigned_to}</td>
			<td>{status}</td>
			<td>{getDate()}</td>
			<td>{isEdit ? updateButtons : changeButtons}</td>
		</tr>
	);
};
