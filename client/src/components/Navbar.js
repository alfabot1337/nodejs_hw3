import { useContext } from 'react';
import { NavLink } from 'react-router-dom';
import { AuthContext } from '../context/AuthContext';

export const Navbar = ({ role, email }) => {
	const auth = useContext(AuthContext);
	const logoutHandler = event => {
		event.preventDefault();
		auth.logout();
	};

	if (role === 'DRIVER') {
		return (
			<nav className="navbar navbar-expand-lg navbar-light">
				<NavLink className="navbar-brand" to="/users/me">
					Profile
				</NavLink>
				<div className="collapse navbar-collapse" id="navbarNavAltMarkup">
					<ul className="navbar-nav">
						<li className="nav-item">
							<NavLink className="nav-link" to="/trucks/add">
								Add truck
							</NavLink>
						</li>
						<li className="nav-item">
							<NavLink className="nav-link" to="/trucks">
								Trucks
							</NavLink>
						</li>
						<li className="nav-item">
							<NavLink className="nav-link" to="/loads/active">
								Active loads
							</NavLink>
						</li>
					</ul>
					<div className="ml-auto header_right">
						<span>Hello, {email}</span>
						<NavLink
							className="nav-link text-warning ml-5"
							to="/"
							onClick={logoutHandler}
						>
							Logout
						</NavLink>
					</div>
				</div>
			</nav>
		);
	}

	return (
		<nav className="navbar navbar-expand-lg navbar-light">
			<NavLink className="navbar-brand" to="/users/me">
				Profile
			</NavLink>
			<div className="collapse navbar-collapse" id="navbarNavAltMarkup">
				<ul className="navbar-nav">
					<li className="nav-item">
						<NavLink className="nav-link" to="/loads/create">
							Create load
						</NavLink>
					</li>
					<li className="nav-item">
						<NavLink className="nav-link" to="/loads">
							New loads
						</NavLink>
					</li>
					<li className="nav-item">
						<NavLink className="nav-link" to="/loads/posted">
							Posted loads
						</NavLink>
					</li>
					<li className="nav-item">
						<NavLink className="nav-link" to="/loads/assigned">
							Assigned loads
						</NavLink>
					</li>
					<li className="nav-item">
						<NavLink className="nav-link" to="/loads/history">
							History
						</NavLink>
					</li>
				</ul>
				<div className="ml-auto header_right">
					<span>Hello, {email}</span>
					<NavLink
						className="nav-link text-warning ml-5"
						to="/"
						onClick={logoutHandler}
					>
						Logout
					</NavLink>
				</div>
			</div>
		</nav>
	);
};
