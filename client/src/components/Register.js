import { useContext, useState } from 'react';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';
import { ErrorMessage } from './ErrorMessage';

export const Register = () => {
	const auth = useContext(AuthContext);
	const { loading, request, error } = useHttp();
	const [form, setForm] = useState({
		email: '',
		password: '',
	});
	const [role, setRole] = useState('shipper');

	const changeHandler = event => {
		setForm({ ...form, [event.target.name]: event.target.value });
	};

	const loginHandler = async () => {
		try {
			const data = await request('/api/auth/login', 'POST', { ...form });
			auth.login(data.jwt_token);
		} catch (e) {}
	};

	const registerHandler = async () => {
		try {
			await request('/api/auth/register', 'POST', { ...form, role });
			loginHandler();
		} catch (e) {}
	};

	const roleHandler = e => {
		setRole(e.target.value);
	};

	return (
		<div className="row">
			<div className="col-md-6 offset-md-3 pt-4">
				<h3 className="mb-1 text-center">Register</h3>
				<form>
					<div className="form-group">
						<label htmlFor="email">Email</label>
						<input
							name="email"
							id="email"
							type="email"
							className="form-control"
							placeholder="Email"
							value={form.email}
							onChange={changeHandler}
						/>
					</div>
					<div className="form-group">
						<label htmlFor="password">Password</label>
						<input
							name="password"
							id="password"
							type="password"
							className="form-control"
							placeholder="Password"
							value={form.password}
							onChange={changeHandler}
						/>
					</div>
					<select className="custom-select" onChange={roleHandler} value={role}>
						<option value="shipper">Shipper</option>
						<option value="driver">Driver</option>
					</select>
					<button
						type="button"
						className="btn btn-success btn-block mt-3"
						disabled={loading}
						onClick={registerHandler}
					>
						Sign up
					</button>
				</form>

				{error && <ErrorMessage text={error} />}
			</div>
		</div>
	);
};
