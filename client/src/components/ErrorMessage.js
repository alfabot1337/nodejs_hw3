export const ErrorMessage = ({ text }) => {
	return (
		<div className="alert alert-danger mt-4 error-message" role="alert">
			{text}
		</div>
	);
};
