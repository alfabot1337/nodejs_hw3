import { useCallback, useContext, useEffect, useState } from 'react';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';
import { Load } from './Load';

export const ActiveLoads = () => {
	const { request, loading } = useHttp();
	const { token } = useContext(AuthContext);
	const [load, setLoad] = useState({});

	const getLoads = useCallback(async () => {
		const data = await request('/api/loads/active', 'GET', null, {
			Authorization: `Bearer ${token}`,
		});
		return data;
	}, [request, token]);

	useEffect(() => {
		const fetchData = async () => {
			const { load } = await getLoads();
			setLoad(load);
		};
		fetchData();
	}, [getLoads]);

	if (loading) {
		return <h2>Loading, please wait...</h2>;
	}

	const renderLoads = () =>
		load.map(i => <Load load={i} key={i._id} />).reverse();

	if (load && !load.length) {
		return <p>Here is clear</p>;
	}

	return (
		<>
			<table className="table">
				<thead>
					<tr>
						<th scope="row">Load name</th>
						<th scope="row">Created date</th>
						<th scope="row">Pick-up address</th>
						<th scope="row">Delivery address</th>
						<th scope="row">Status</th>
						<th scope="row">State</th>
						<th scope="row"></th>
					</tr>
				</thead>
				<tbody>{renderLoads()}</tbody>
			</table>
		</>
	);
};
