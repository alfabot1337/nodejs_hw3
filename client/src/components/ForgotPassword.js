import { useState } from 'react';
import { useHttp } from '../hooks/http.hook';
import { ErrorMessage } from './ErrorMessage';

export const ForgotPassword = () => {
	const { loading, request, error } = useHttp();
	const [form, setForm] = useState({
		email: '',
		newPassword: '',
	});

	const changeHandler = event => {
		setForm({ ...form, [event.target.name]: event.target.value });
	};

	const forgotPassHandler = async () => {
		try {
			const response = await request('/api/users/me/restore', 'PATCH', {
				...form,
			});
			console.log(response);
		} catch (e) {}
	};

	return (
		<div className="row">
			<div className="col-md-6 offset-md-3 pt-4">
				<h5 className="mb-1 text-center">
					Type your credentials to reset password
				</h5>
				<div className="form-group">
					<label htmlFor="email">Email</label>
					<input
						name="email"
						id="email"
						type="email"
						className="form-control"
						placeholder="Email"
						value={form.email}
						onChange={changeHandler}
					/>
				</div>
				<div className="form-group">
					<label htmlFor="email">New password</label>
					<input
						name="newPassword"
						id="newPassword"
						type="password"
						className="form-control"
						placeholder="New Password"
						value={form.newPassword}
						onChange={changeHandler}
					/>
				</div>
				<button
					type="submit"
					className="btn btn-primary btn-block"
					disabled={loading}
					onClick={forgotPassHandler}
				>
					Submit
				</button>
				{error && <ErrorMessage text={error} />}
			</div>
		</div>
	);
};
