import { useContext, useEffect, useState } from 'react';
// import { ErrorMessage } from '../components/ErrorMessage';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';
import { ErrorMessage } from './ErrorMessage';
import { ForgotPassword } from './ForgotPassword';

export const Login = () => {
	const auth = useContext(AuthContext);
	const { loading, request, error, clearError } = useHttp();
	const [form, setForm] = useState({
		email: '',
		password: '',
	});
	const [isForgotPass, setIsForgotPass] = useState(false);

	useEffect(() => {
		if (error) {
			setTimeout(() => {
				clearError();
			}, 3000);
		}
	}, [error, clearError]);

	const changeHandler = event => {
		setForm({ ...form, [event.target.name]: event.target.value });
	};

	const loginHandler = async () => {
		try {
			const data = await request('/api/auth/login', 'POST', { ...form });
			auth.login(data.jwt_token);
		} catch (e) {}
	};

	if (isForgotPass) {
		return <ForgotPassword />;
	}

	return (
		<div className="row">
			<div className="col-md-6 offset-md-3 pt-4">
				<h3 className="mb-1 text-center">Join</h3>
				<form>
					<div className="form-group">
						<label htmlFor="email">Email</label>
						<input
							name="email"
							id="email"
							type="email"
							className="form-control"
							placeholder="Email"
							value={form.email}
							onChange={changeHandler}
						/>
					</div>
					<div className="form-group">
						<label htmlFor="password">Password</label>
						<input
							name="password"
							id="password"
							type="password"
							className="form-control"
							placeholder="Password"
							value={form.password}
							onChange={changeHandler}
						/>
					</div>
					<button
						type="submit"
						className="btn btn-primary btn-block"
						disabled={loading}
						onClick={loginHandler}
					>
						Sign in
					</button>
					<button
						type="button"
						className="btn btn-secondary mt-1 btn-block"
						disabled={loading}
						onClick={() => setIsForgotPass(true)}
					>
						Forgot password
					</button>
				</form>

				{error && <ErrorMessage text={error} />}
			</div>
		</div>
	);
};
