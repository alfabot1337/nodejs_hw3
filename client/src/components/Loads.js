import { useCallback, useContext, useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';
import { Load } from './Load';

export const Loads = ({ filter = 'ALL' }) => {
	const { request, loading } = useHttp();
	const { token } = useContext(AuthContext);
	const [loads, setLoads] = useState([]);

	const getLoads = useCallback(async () => {
		const data = await request('/api/loads', 'GET', null, {
			Authorization: `Bearer ${token}`,
		});
		return data;
	}, [request, token]);

	const filterLoads = useCallback(
		arr => (filter === 'ALL' ? arr : arr.filter(i => i.status === filter)),
		[filter]
	);

	useEffect(() => {
		const fetchData = async () => {
			const { loads } = await getLoads();
			setLoads(filterLoads(loads));
		};
		fetchData();
	}, [getLoads, filterLoads]);

	if (loading) {
		return <h2>Loading, please wait...</h2>;
	}

	const renderLoads = () =>
		loads.map(i => <Load load={i} key={i._id} />).reverse();

	if (loads && !loads.length) {
		return (
			<p>
				Here is clear, you can add your own load
				<NavLink to="/loads/create"> here</NavLink>
			</p>
		);
	}

	return (
		<>
			<table className="table">
				<thead>
					<tr>
						<th scope="row">Load name</th>
						<th scope="row">Created date</th>
						<th scope="row">Pick-up address</th>
						<th scope="row">Delivery address</th>
						<th scope="row">Status</th>
						<th scope="row"></th>
					</tr>
				</thead>
				<tbody>{renderLoads()}</tbody>
			</table>
		</>
	);
};
