import { useContext, useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { ErrorMessage } from '../components/ErrorMessage';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';

export const LoadPage = ({ isEdit = false }) => {
	const id = useParams().id;
	const { request, loading, error } = useHttp();
	const { token } = useContext(AuthContext);
	const history = useHistory();
	const [form, setForm] = useState({
		name: '',
		payload: 0,
		pickup_address: '',
		delivery_address: '',
		logs: [],
	});
	const [dimensions, setDimensions] = useState({
		width: 0,
		length: 0,
		height: 0,
	});

	const formChangeHandler = event => {
		setForm({ ...form, [event.target.name]: event.target.value });
	};
	const dimensionsChangeHandler = event => {
		if (!event.target.validity.valid) {
			return;
		}
		setDimensions({
			...dimensions,
			[event.target.name]: event.target.value,
		});
	};

	const submitForm = async event => {
		event.preventDefault();
		try {
			await request(
				`/api/loads/${id}`,
				'PUT',
				{ ...form, dimensions },
				{
					Authorization: `Bearer ${token}`,
				}
			);
			history.push('/loads');
		} catch (e) {}
	};

	useEffect(() => {
		const fetchLoad = async () => {
			const {
				load: { dimensions, ...fetchedLoad },
			} = await request(`/api/loads/${id}`, 'GET', null, {
				Authorization: `Bearer ${token}`,
			});
			setForm(fetchedLoad);
			setDimensions(dimensions);
		};
		fetchLoad();
	}, [id, request, token]);

	const getDate = () => {
		const date = new Date(Date.parse(form.created_date));
		return `${date.toDateString()} ${date.getHours()}:${date.getMinutes()}`;
	};

	const deleteLoad = async () => {
		try {
			await request(`/api/loads/${id}`, 'DELETE', null, {
				Authorization: `Bearer ${token}`,
			});
			history.push('/loads');
		} catch (error) {}
	};

	if (isEdit && form.status !== 'NEW') {
		return 'You can not change load now';
	}

	if (isEdit) {
		return (
			<div>
				<h2>Change load</h2>
				<div className="row">
					<div className="col-md-6 offset-md-3">
						<form onSubmit={submitForm}>
							<div className="form-group">
								<label htmlFor="name">Name</label>
								<input
									name="name"
									id="name"
									type="text"
									className="form-control"
									placeholder="Name"
									value={form.name}
									onChange={formChangeHandler}
									required={true}
								/>
							</div>
							<div className="form-group">
								<label htmlFor="payload">Payload</label>
								<input
									name="payload"
									id="payload"
									type="number"
									className="form-control"
									placeholder="Payload"
									min="0"
									value={form.payload}
									onChange={formChangeHandler}
									required={true}
								/>
							</div>
							<div className="form-group">
								<label htmlFor="pickup_address">Pickup address</label>
								<input
									name="pickup_address"
									id="pickup_address"
									type="text"
									className="form-control"
									placeholder="Pickup address"
									value={form.pickup_address}
									onChange={formChangeHandler}
									required={true}
								/>
							</div>
							<div className="form-group">
								<label htmlFor="delivery_address">Delivery address</label>
								<input
									name="delivery_address"
									id="delivery_address"
									type="text"
									className="form-control"
									placeholder="Delivery address"
									value={form.delivery_address}
									onChange={formChangeHandler}
									required={true}
								/>
							</div>
							<div className="form-row">
								<div className="form-group col">
									<label htmlFor="width">Width</label>
									<input
										name="width"
										id="width"
										type="number"
										className="form-control"
										placeholder="Width"
										min="0"
										value={dimensions.width}
										onChange={dimensionsChangeHandler}
										required={true}
									/>
								</div>
								<div className="form-group col">
									<label htmlFor="height">Height</label>
									<input
										name="height"
										id="height"
										type="number"
										className="form-control"
										placeholder="Height"
										min="0"
										value={dimensions.height}
										onChange={dimensionsChangeHandler}
										required={true}
									/>
								</div>
								<div className="form-group col">
									<label htmlFor="length">Length</label>
									<input
										name="length"
										id="length"
										type="number"
										className="form-control"
										placeholder="Length"
										min="0"
										value={dimensions.length}
										onChange={dimensionsChangeHandler}
										required={true}
									/>
								</div>
							</div>
							<button
								type="submit"
								className="btn btn-primary mr-2"
								disabled={loading}
							>
								Change
							</button>
							<button
								className="btn btn-danger"
								disabled={loading}
								onClick={deleteLoad}
							>
								Delete load
							</button>
						</form>
					</div>
				</div>
				{error && <ErrorMessage text={error} />}
			</div>
		);
	}

	return (
		<div>
			<h2>View load</h2>
			<div className="row">
				<div className="col-md-6 offset-md-3">
					<p>
						<span>Name: </span>
						<span>{form.name}</span>
					</p>
					<p>
						<span>Pickup address: </span>
						<span>{form.pickup_address}</span>
					</p>
					<p>
						<span>Delivery address: </span>
						<span>{form.delivery_address}</span>
					</p>
					<p>
						<span>Payload: </span>
						<span>{form.payload}</span>
					</p>
					<p>
						<span>Width: </span>
						<span>{dimensions.width}, </span>
						<span>Height: </span>
						<span>{dimensions.height} </span>
						<span>Length: </span>
						<span>{dimensions.length}</span>
					</p>
					<p>
						<span>Status: </span>
						<span>{form.status}</span>
					</p>
					<p>
						<span>State: </span>
						<span>{form.state || 'Unknown'}</span>
					</p>
					<p>
						<span>Created date: </span>
						<span>{getDate()}</span>
					</p>
				</div>
			</div>
		</div>
	);
};
