import React, { useState } from 'react';
import { Login } from '../components/Login';
import { Register } from '../components/Register';

export const AuthPage = () => {
	const [action, setAction] = useState('login');

	const changeActionHandler = e => {
		setAction(e.target.name);
	};

	return (
		<div className="auth-page">
			<div className="auth-page__options">
				<button
					type="button"
					className="change-button btn btn-primary"
					name="login"
					onClick={changeActionHandler}
					disabled={action === 'login'}
				>
					Login
				</button>
				<button
					type="button"
					className="change-button btn btn-primary"
					name="register"
					onClick={changeActionHandler}
					disabled={action === 'register'}
				>
					Register
				</button>
			</div>
			{action === 'login' ? <Login /> : <Register />}
		</div>
	);
};
