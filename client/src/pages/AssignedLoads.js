import { Loads } from '../components/Loads';

export const AssignedLoads = () => {
	return (
		<>
			<h2>Assigned loads</h2>
			<Loads filter={'ASSIGNED'} />
		</>
	);
};
