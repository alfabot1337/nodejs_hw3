import { Loads } from '../components/Loads';

export const AllLoads = () => {
	return (
		<>
			<h2>History</h2>
			<Loads filter={'ALL'} />
		</>
	);
};
