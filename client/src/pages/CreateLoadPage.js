import { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { ErrorMessage } from '../components/ErrorMessage';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';

export const CreateLoadPage = () => {
	const { request, loading, error } = useHttp();
	const auth = useContext(AuthContext);
	const history = useHistory();
	const [form, setForm] = useState({
		name: '',
		payload: 0,
		pickup_address: '',
		delivery_address: '',
	});
	const [dimensions, setDimensions] = useState({
		width: 0,
		length: 0,
		height: 0,
	});

	const formChangeHandler = event => {
		setForm({ ...form, [event.target.name]: event.target.value });
	};
	const dimensionsChangeHandler = event => {
		if (!event.target.validity.valid) {
			return;
		}
		setDimensions({
			...dimensions,
			[event.target.name]: event.target.value,
		});
	};

	const createLoad = async () => {
		try {
			await request(
				'/api/loads',
				'POST',
				{ ...form, dimensions },
				{
					Authorization: `Bearer ${auth.token}`,
				}
			);
		} catch (e) {}
	};

	const postLoad = async id => {
		try {
			const response = await request(`/api/loads/${id}/post`, 'POST', null, {
				Authorization: `Bearer ${auth.token}`,
			});
			return response;
		} catch (error) {}
	};

	const getCreatedLoad = async () => {
		try {
			const { loads } = await request(`/api/loads`, 'GET', null, {
				Authorization: `Bearer ${auth.token}`,
			});
			return loads.pop();
		} catch (error) {}
	};

	const submitForm = async event => {
		event.preventDefault();
		await createLoad();
		const { _id } = await getCreatedLoad();
		const response = await postLoad(_id);
		if (response.driver_found) {
			alert('We found a driver for you');
		} else {
			alert(
				'Sorry, we cant find a driver for you. We will try to find a car harder'
			);
		}
		history.push('/loads');
	};

	return (
		<div>
			<h2>Create load</h2>
			<div className="row">
				<div className="col-md-6 offset-md-3">
					<form onSubmit={submitForm}>
						<div className="form-group">
							<label htmlFor="name">Name</label>
							<input
								name="name"
								id="name"
								type="text"
								className="form-control"
								placeholder="Name"
								value={form.name}
								onChange={formChangeHandler}
								required={true}
							/>
						</div>
						<div className="form-group">
							<label htmlFor="payload">Payload</label>
							<input
								name="payload"
								id="payload"
								type="number"
								className="form-control"
								placeholder="Payload"
								min="0"
								value={form.payload}
								onChange={formChangeHandler}
								required={true}
							/>
						</div>
						<div className="form-group">
							<label htmlFor="pickup_address">Pickup address</label>
							<input
								name="pickup_address"
								id="pickup_address"
								type="text"
								className="form-control"
								placeholder="Pickup address"
								value={form.pickup_address}
								onChange={formChangeHandler}
								required={true}
							/>
						</div>
						<div className="form-group">
							<label htmlFor="delivery_address">Delivery address</label>
							<input
								name="delivery_address"
								id="delivery_address"
								type="text"
								className="form-control"
								placeholder="Delivery address"
								value={form.delivery_address}
								onChange={formChangeHandler}
								required={true}
							/>
						</div>
						<div className="form-row">
							<div className="form-group col">
								<label htmlFor="width">Width</label>
								<input
									name="width"
									id="width"
									type="number"
									className="form-control"
									placeholder="Width"
									min="0"
									value={dimensions.width}
									onChange={dimensionsChangeHandler}
									required={true}
								/>
							</div>
							<div className="form-group col">
								<label htmlFor="height">Height</label>
								<input
									name="height"
									id="height"
									type="number"
									className="form-control"
									placeholder="Height"
									min="0"
									value={dimensions.height}
									onChange={dimensionsChangeHandler}
									required={true}
								/>
							</div>
							<div className="form-group col">
								<label htmlFor="length">Length</label>
								<input
									name="length"
									id="length"
									type="number"
									className="form-control"
									placeholder="Length"
									min="0"
									value={dimensions.length}
									onChange={dimensionsChangeHandler}
									required={true}
								/>
							</div>
						</div>
						<button
							type="submit"
							className="btn btn-primary btn-block"
							disabled={loading}
						>
							Create
						</button>
					</form>
				</div>
			</div>
			{error && <ErrorMessage text={error} />}
		</div>
	);
};
