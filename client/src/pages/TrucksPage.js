import { Trucks } from '../components/Trucks';

export const TrucksPage = () => {
	return (
		<>
			<h2>Your trucks</h2>
			<Trucks />
		</>
	);
};
