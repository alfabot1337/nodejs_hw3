import { ActiveLoads } from '../components/ActiveLoads';

export const ActiveLoadsPage = () => {
	return (
		<>
			<h2>Active Loads</h2>
			<ActiveLoads />
		</>
	);
};
