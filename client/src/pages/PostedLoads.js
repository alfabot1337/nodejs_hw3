import { Loads } from '../components/Loads';

export const PostedLoads = () => {
	return (
		<>
			<h2>Posted loads</h2>
			<Loads filter={'POSTED'} />
		</>
	);
};
