import { Loads } from '../components/Loads';

export const NewLoads = () => {
	return (
		<>
			<h2>New loads</h2>
			<Loads filter={'NEW'} />
		</>
	);
};
