import { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { AuthContext } from '../context/AuthContext';
import { useHttp } from '../hooks/http.hook';

export const CreateTruckPage = () => {
	const { request } = useHttp();
	const auth = useContext(AuthContext);
	const history = useHistory();
	const [type, setType] = useState('SPRINTER');

	const typeHandler = e => {
		setType(e.target.value);
	};

	const createTruckHandler = async () => {
		try {
			await request(
				'/api/trucks',
				'POST',
				{ type },
				{
					Authorization: `Bearer ${auth.token}`,
				}
			);
			history.push('/trucks');
		} catch (e) {}
	};

	return (
		<>
			<h2>Add truck</h2>
			<select className="custom-select" onChange={typeHandler} value={type}>
				<option value="SPRINTER">Sprinter</option>
				<option value="SMALL STRAIGHT">Small Straight</option>
				<option value="LARGE STRAIGHT">Large Straight</option>
			</select>
			<button
				type="button"
				className="btn btn-success btn-block mt-3"
				onClick={createTruckHandler}
			>
				Add
			</button>
		</>
	);
};
