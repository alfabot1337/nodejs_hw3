import { BrowserRouter as Router } from 'react-router-dom';
import { Navbar } from './components/Navbar';
import { useAuth } from './hooks/auth.hook';
import { useRoutes } from './routes';
import { AuthContext } from './context/AuthContext';
import './App.css';

function App() {
	let { token, login, logout, role, email } = useAuth();
	const isAuthenticated = !!token;
	const routes = useRoutes(isAuthenticated, role);

	return (
		<AuthContext.Provider
			value={{ token, login, logout, isAuthenticated, role }}
		>
			<Router>
				{isAuthenticated && <Navbar role={role} email={email} />}
				<div className="container">{routes}</div>
			</Router>
		</AuthContext.Provider>
	);
}

export default App;
