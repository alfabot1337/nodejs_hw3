const { model, Schema } = require('mongoose');

module.exports = model(
	'load',
	new Schema({
		name: String,
		created_by: {
			type: String,
			required: true,
		},
		logs: Array,
		assigned_to: String,
		pickup_address: String,
		delivery_address: String,
		status: String,
		state: String,
		dimensions: {
			width: Number,
			length: Number,
			height: Number,
		},
		payload: Number,
		created_date: Date,
	})
);
