const { model, Schema } = require('mongoose');

module.exports = model(
	'truck',
	new Schema({
		created_by: {
			type: String,
			required: true,
		},
		assigned_to: String,
		status: String,
		type: String,
		created_date: Date,
	})
);
