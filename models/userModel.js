const { model, Schema } = require('mongoose');

module.exports = model(
	'user',
	new Schema({
		email: {
			type: String,
			required: true,
			unique: true,
		},
		password: {
			type: String,
			required: true,
		},
		role: {
			type: String,
			required: true,
		},
		created_date: {
			type: Date,
		},
	})
);
