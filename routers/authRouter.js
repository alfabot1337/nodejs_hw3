const { Router } = require('express');
const router = Router();

const {
	register,
	login,
	forgotPassword,
} = require('../controllers/authController');

router.post('/auth/register', register);
router.post('/auth/login', login);
router.post('/auth/forgot_password', forgotPassword);

module.exports = router;
