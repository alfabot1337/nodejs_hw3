const { Router } = require('express');
const {
	userInfo,
	changePassword,
	deleteUser,
} = require('../controllers/userController');
const authMiddleware = require('../middlewares/authMiddleware');
const router = Router();

router.get('/users/me', authMiddleware, userInfo);
router.patch('/users/me/password', authMiddleware, changePassword);
router.delete('/users/me', authMiddleware, deleteUser);

module.exports = router;
