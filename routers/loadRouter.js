const { Router } = require('express');
const {
	addLoad,
	getLoads,
	activeLoads,
	getActiveLoadNextState,
	getLoadById,
	updateLoadById,
	deleteLoadById,
	postLoadById,
	getShippingInfo,
} = require('../controllers/loadController');
const authMiddleware = require('../middlewares/authMiddleware');
const driverMiddleware = require('../middlewares/driverMiddleware');
const shipperMiddleware = require('../middlewares/shipperMiddleware');
const router = Router();

router.get('/loads', authMiddleware, getLoads);
router.post('/loads', authMiddleware, shipperMiddleware, addLoad);
router.get('/loads/active', authMiddleware, driverMiddleware, activeLoads);
router.patch(
	'/loads/active/state',
	authMiddleware,
	driverMiddleware,
	getActiveLoadNextState
);
router.get('/loads/:id', authMiddleware, getLoadById);
router.put('/loads/:id', authMiddleware, shipperMiddleware, updateLoadById);
router.delete('/loads/:id', authMiddleware, shipperMiddleware, deleteLoadById);
router.post('/loads/:id/post', authMiddleware, shipperMiddleware, postLoadById);
router.get(
	'/loads/:id/shipping_info',
	authMiddleware,
	shipperMiddleware,
	getShippingInfo
);

module.exports = router;
