const { Router } = require('express');
const {
	createTruck,
	getTrucks,
	getTruckById,
	updateTruckById,
	deleteTruckById,
	assignTruck,
} = require('../controllers/truckController');
const authMiddleware = require('../middlewares/authMiddleware');
const driverMiddleware = require('../middlewares/driverMiddleware');

const router = Router();

router.get('/trucks', authMiddleware, driverMiddleware, getTrucks);
router.post('/trucks', authMiddleware, driverMiddleware, createTruck);
router.get('/trucks/:id', authMiddleware, driverMiddleware, getTruckById);
router.put('/trucks/:id', authMiddleware, driverMiddleware, updateTruckById);
router.delete('/trucks/:id', authMiddleware, driverMiddleware, deleteTruckById);
router.post(
	'/trucks/:id/assign',
	authMiddleware,
	driverMiddleware,
	assignTruck
);

module.exports = router;
