const User = require('../models/userModel');
const bcrypt = require('bcrypt');

module.exports.userInfo = async (req, res) => {
	await User.findById(req.user.userId)
		.exec()
		.then(user => {
			res.status(200).json(user);
		})
		.catch(err => res.status(400).json({ message: err.message }));
};

module.exports.changePassword = (req, res) => {
	const { oldPassword, newPassword } = req.body;
	User.findById(req.user.userId)
		.exec()
		.then(async user => {
			const isMatch = await bcrypt.compare(oldPassword, user.password);
			console.log(oldPassword, user.password);
			if (!isMatch) {
				return res.status(400).json({ message: 'Old password is incorrect' });
			}
			const isSamePassword = await bcrypt.compare(newPassword, user.password);
			if (isSamePassword) {
				return res.status(400).json({
					message: 'You are trying to use the same password',
				});
			}

			const hashedPassword = await bcrypt.hash(newPassword, 10);
			User.findByIdAndUpdate(user._id, { password: hashedPassword })
				.exec()
				.then(() => {
					res.status(200).json({ message: 'Password changed successfully' });
				})
				.catch(err => res.status(400).json({ message: err.message }));
		})
		.catch(() => res.status(400).json({ message: 'User is incorrect' }));
};

module.exports.deleteUser = (req, res) => {
	User.findOneAndDelete({ _id: req.user.userId })
		.then(() => {
			res.status(200).json({ message: 'Profile deleted successfully' });
		})
		.catch(err => res.status(400).json({ message: err.message }));
};
