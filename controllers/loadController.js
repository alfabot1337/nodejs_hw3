const Load = require('../models/loadModel');
const Truck = require('../models/truckModel');
const User = require('../models/userModel');
const { SHIPPER, DRIVER } = require('../roles/roles');
const { enRouteToPickUp, arrivedToDelivery } = require('../states/loadStates');
const loadStates = require('../states/loadStates');
const { NEW, ASSIGNED, SHIPPED, POSTED } = require('../statuses/loadStatuses');
const { inService, onLoad } = require('../statuses/truckStatuses');
const truckTypes = require('../types/truckTypes');

module.exports.addLoad = (req, res) => {
	const {
		name,
		payload,
		pickup_address,
		delivery_address,
		dimensions,
	} = req.body;

	const load = new Load({
		name,
		created_by: req.user.userId,
		logs: [],
		pickup_address,
		delivery_address,
		status: NEW,
		dimensions,
		payload,
		created_date: Date.now(),
	});

	load
		.save()
		.then(() => {
			res.status(200).json({ message: 'Load created successfully' });
		})
		.catch(err => {
			res.status(400).json({ message: err.message });
		});
};

module.exports.getLoads = async (req, res) => {
	const id = req.user.userId;
	const user = await User.findById(id);

	if (user.role === SHIPPER) {
		Load.find({ created_by: id })
			.then(loads => {
				return res.status(200).json({ loads });
			})
			.catch(err => {
				return res.status(400).json({ message: err.message });
			});
	} else if (user.role === DRIVER) {
		Load.find({ assigned_to: id })
			.then(loads => {
				loads = loads.filter(
					load => load.status === ASSIGNED || load.status === SHIPPED
				);
				res.status(200).json({ loads });
			})
			.catch(err => {
				return res.status(400).json({ message: err.message });
			});
	}
};

module.exports.activeLoads = (req, res) => {
	const id = req.user.userId;

	Load.find({ assigned_to: id, status: ASSIGNED })
		.then(load => res.status(200).json({ load }))
		.catch(err => res.status(400).json({ message: err.message }));
};

module.exports.getActiveLoadNextState = async (req, res) => {
	const id = req.user.userId;

	const load = await Load.findOne({ assigned_to: id });

	const currIdx = Object.entries(loadStates).findIndex(
		i => i[1] === load.state
	);
	const nextState = Object.entries(loadStates)[currIdx + 1][1];

	if (nextState === arrivedToDelivery) {
		load.status = SHIPPED;
		await Truck.findOneAndUpdate(
			{ assigned_to: load.assigned_to },
			{ status: inService, assigned_to: null }
		);
	}
	const message = `Load state changed to '${nextState}'`;

	load.state = nextState;
	load.logs.push({ message, time: Date.now() });
	load.save().then(() => res.status(200).json({ message }));
};

module.exports.getLoadById = (req, res) => {
	Load.findById(req.params.id)
		.then(load => res.status(200).json({ load }))
		.catch(err => res.status(400).json({ message: err.message }));
};

module.exports.updateLoadById = (req, res) => {
	const {
		name,
		payload,
		pickup_address,
		delivery_address,
		dimensions,
	} = req.body;

	Load.findByIdAndUpdate(req.params.id, {
		name,
		payload,
		pickup_address,
		delivery_address,
		dimensions,
	})
		.then(() =>
			res.status(200).json({ message: 'Load details changed successfully' })
		)
		.catch(err => res.status(400).json({ message: err.message }));
};

module.exports.deleteLoadById = (req, res) => {
	Load.findByIdAndDelete(req.params.id)
		.then(() => res.status(200).json({ message: 'Load deleted successfully' }))
		.catch(err => res.status(400).json({ message: err.message }));
};

module.exports.postLoadById = async (req, res) => {
	const loadId = req.params.id;

	let load = await Load.findByIdAndUpdate(loadId, { status: POSTED });

	const truck = await Truck.find({ status: inService }).then(trucks => {
		return trucks.find(({ type, assigned_to }) => {
			const { dimensions } = load;
			const { width, length, height, payload } = truckTypes[type];

			return (
				assigned_to,
				dimensions.width < width &&
					dimensions.length < length &&
					dimensions.height < height &&
					load.payload < payload
			);
		});
	});

	if (!truck || load.assigned_to) {
		const message = `Load status changed to '${NEW}'`;
		await Load.findByIdAndUpdate(loadId, {
			status: NEW,
			logs: [...load.logs, { message, time: Date.now() }],
		});
		console.log('changed');
		return res.status(200).json({
			message: "Can't find driver for your load",
			driver_found: false,
		});
	}

	truck.status = onLoad;
	await truck.save();

	const message = `Load state changed to '${enRouteToPickUp}'`;
	Load.findByIdAndUpdate(loadId, {
		assigned_to: truck.created_by,
		status: ASSIGNED,
		state: enRouteToPickUp,
		logs: [
			...load.logs,
			{ message, time: Date.now() },
			{
				message: `Load assigned to driver with id ${truck.created_by}`,
				time: Date.now(),
			},
		],
	}).then(() =>
		res.status(200).json({
			message: 'Load posted successfully',
			driver_found: true,
		})
	);
};

module.exports.getShippingInfo = async (req, res) => {
	const loadId = req.params.id;

	try {
		const load = await Load.findById(loadId);
		if (load && !load.assigned_to) {
			return res.status(400).json({ message: 'Your load is not assigned' });
		}
		const truck = await Truck.find({
			created_by: load.assigned_to,
			status: onLoad,
		});
		res.status(200).json({ load, truck });
	} catch (err) {
		res.status(400).json({ message: err.message });
	}
};
