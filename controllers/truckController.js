const Truck = require('../models/truckModel');
const { inService, onLoad } = require('../statuses/truckStatuses');
const truckTypes = require('../types/truckTypes');

module.exports.createTruck = (req, res) => {
	let { type } = req.body;
	type = type.toUpperCase();
	const notExistsType = !truckTypes[type];
	if (notExistsType) {
		return res
			.status(400)
			.json({ message: 'This type of truck are not exists' });
	}

	const created_date = Date.now();
	const truck = new Truck({
		created_by: req.user.userId,
		status: inService,
		type,
		created_date,
	});

	truck
		.save()
		.then(() => {
			res.status(200).json({ message: 'Truck created successfully' });
		})
		.catch(err => {
			res.status(400).json({ message: err.message });
		});
};

module.exports.getTrucks = (req, res) => {
	Truck.find({ created_by: req.user.userId })
		.then(trucks => {
			if (!trucks) {
				return res
					.status(400)
					.json({ message: 'Cant find truck with this id' });
			}
			res.status(200).json({ trucks });
		})
		.catch(err => res.status(400).json({ message: err.message }));
};

module.exports.getTruckById = (req, res) => {
	Truck.findById(req.params.id)
		.then(truck => res.status(200).json({ truck }))
		.catch(err => res.status(400).json({ message: err.message }));
};

module.exports.updateTruckById = (req, res) => {
	let { type } = req.body;
	type = type.toUpperCase();
	const notExistsType = !truckTypes[type];
	if (notExistsType) {
		return res
			.status(400)
			.json({ message: 'This type of truck are not exists' });
	}

	Truck.findByIdAndUpdate(req.params.id, { type })
		.then(() => {
			res.status(200).json({ message: 'Truck details changed successfully' });
		})
		.catch(err => {
			res.status(400).json({ message: err.message });
		});
};

module.exports.deleteTruckById = (req, res) => {
	Truck.findByIdAndDelete(req.params.id)
		.then(() => {
			res.status(200).json({ message: 'Truck deleted successfully' });
		})
		.catch(err => {
			res.status(400).json({ message: err.message });
		});
};

module.exports.assignTruck = async (req, res) => {
	const trucks = await Truck.find({ assigned_to: req.user.userId });
	if (trucks.length) {
		return res
			.status(400)
			.json({ message: 'You already have assigned to you trucks' });
	}

	Truck.findByIdAndUpdate(req.params.id, { assigned_to: req.user.userId })
		.then(truck => {
			if (!truck) {
				return res
					.status(400)
					.json({ message: 'Cant find truck with this id' });
			}
			res.status(200).json({ message: 'Truck assigned successfully' });
		})
		.catch(err => {
			res.status(400).json({ message: err.message });
		});
};
