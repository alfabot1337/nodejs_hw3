const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const config = require('config');
const nodemailer = require('nodemailer');
const generator = require('generate-password');
const User = require('../models/userModel');
const roles = require('../roles/roles');

module.exports.register = async (req, res) => {
	const { email, password } = req.body;
	let { role } = req.body;

	role = role.toUpperCase();
	const notExistsRole = !roles[role];

	if (notExistsRole) {
		return res.status(400).json({ message: 'This role are not exists' });
	}

	const created_date = Date.now();
	const hashedPassword = await bcrypt.hash(password, 10);
	const user = new User({
		email,
		password: hashedPassword,
		role: role.toUpperCase(),
		created_date,
	});

	user
		.save()
		.then(() => {
			res.status(200).json({ message: 'Profile created successfully' });
		})
		.catch(err => {
			res.status(400).json({ message: err.message });
		});
};

module.exports.login = async (req, res) => {
	const { email, password } = req.body;

	try {
		const user = await User.findOne({ email }).catch(err => {
			res.status(500).json({ message: err.message });
		});

		if (!user) {
			res.status(400).json({ message: "Can't find this user" });
		}

		const isMatch = await bcrypt.compare(password, user.password);

		if (!isMatch) {
			res.status(400).json({ message: 'Username or password is incorrect!' });
		}
		res.status(200).json({
			jwt_token: jwt.sign({ userId: user._id }, config.get('jwtSecret'), {
				expiresIn: '1h',
			}),
		});
	} catch (err) {
		res.status(500).json({ message: err.message });
	}
};

module.exports.forgotPassword = async (req, res) => {
	const { email } = req.body;
	const user = await User.findOne({ email });

	if (!user) {
		res.status(400).json({ message: "Can't find this user" });
	}

	const generatedPassword = await generator.generate({
		length: 10,
		numbers: true,
	});

	const hashedPassword = await bcrypt.hash(generatedPassword, 10);
	User.findByIdAndUpdate(user._id, { password: hashedPassword }).catch(err =>
		res.status(400).json({ message: err.message })
	);

	const transporter = nodemailer.createTransport({
		service: 'gmail',
		auth: {
			user: 'epamnodelab@gmail.com',
			pass: 'epamnodelab1337',
		},
	});
	const mailOptions = {
		from: 'epamnodelab@gmail.com',
		to: email,
		subject: 'Restore password',
		text: `Your new password is '${generatedPassword}'. Please dont forget it!`,
	};

	transporter.sendMail(mailOptions, err => {
		if (err) {
			res.status(500).json({ message: err.message });
		} else {
			res
				.status(200)
				.json({ message: 'New password sent to your email address' });
		}
	});
};
