const express = require('express');
const mongoose = require('mongoose');
const config = require('config');

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const truckRouter = require('./routers/truckRouter');
const loadRouter = require('./routers/loadRouter');

const PORT = process.env.PORT || config.get('port');
const mongoUri = config.get('mongoUri');

const app = express();

async function start() {
	try {
		await mongoose.connect(mongoUri, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useFindAndModify: false,
			useCreateIndex: true,
		});

		app.listen(PORT, () =>
			console.log(`Server has been started on port ${PORT}`)
		);
	} catch (e) {
		console.log('Server error:', e.message);
		process.exit(1);
	}
}

app.use(express.json());
app.use('/api', authRouter);
app.use('/api', userRouter);
app.use('/api', truckRouter);
app.use('/api', loadRouter);

start();
