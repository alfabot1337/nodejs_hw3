const User = require('../models/userModel');
const { SHIPPER } = require('../roles/roles');

module.exports = (req, res, next) => {
	User.findById(req.user.userId)
		.then(user => {
			if (user.role === SHIPPER) {
				return next();
			}
			return res.status(400).json({ message: 'Available only for shipper' });
		})
		.catch(err => {
			res.status(400).json({ message: err.message });
		});
};
