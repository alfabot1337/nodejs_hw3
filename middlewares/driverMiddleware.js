const User = require('../models/userModel');
const { DRIVER } = require('../roles/roles');

module.exports = (req, res, next) => {
	User.findById(req.user.userId)
		.then(user => {
			if (user.role === DRIVER) {
				return next();
			}
			return res.status(400).json({ message: 'Available only for driver' });
		})
		.catch(err => {
			res.status(400).json({ message: err.message });
		});
};
